#!/bin/bash -eua

if [ "$DEBUG" ]; then
	set -x
fi

_SOURCE_SCRIPT_PATH="$(readlink -f "${BASH_SOURCE[0]}")"
_SOURCE_DIR_PATH="$(dirname "$_SOURCE_SCRIPT_PATH")"
cd "$_SOURCE_DIR_PATH"

if [[ ! "$CONTAINERIZED" || "$CONTAINERIZED" = false ]]; then
	source "$_SOURCE_DIR_PATH/.env"
elif [[ "$CONTAINERIZED" || "$CONTAINERIZED" = true ]]; then
  source "$CONFIG_PATH/discord.env"
fi

_PARTS=(
  'lib/log.sh'
  'lib/checks.sh'
  'lib/notify_discord.sh'
  'lib/waiter.sh'
  'bin/env.sh'
  'bin/config.sh'
  'bin/checks.sh'
  'bin/archive.sh'
  'bin/rotate.sh'
  'bin/notify.sh'
)
for _FILE in "${_PARTS[@]}"; do
		source "$_SOURCE_DIR_PATH/$_FILE"
done

WAIT_UNTIL_LOOP
