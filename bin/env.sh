#!/bin/bash -ae

## detect possible operating env misconfiguration

# if ! CHECK_AWS_REGION; then
#     declare -a _WARN_TEXT
#     _WARN_TEXT=(
#         'Operating and service region mismatch:'
#         "$_EC2_REGION != $_SNS_REGION"
#     )
#     _LOG warn "${_WARN_TEXT[@]}"
# fi

## detect script misconfigurations

if
    [ ${#DISCORD_TOKEN} -eq 0 ] \
    || [[
        ${#DISCORD_GUILD_ID} -eq 0
        || ${#DISCORD_WEBHOOK_URI} -eq 0
        || ${#RUN_INTERVAL} -eq 0
    ]]
then
    _USAGE 'Missing required configuration option(s)'
fi

if [[ ! "$CONTAINERIZED" || "$CONTAINERIZED" = false ]]; then
    if [ ${#LOG_PATH} -eq 0 ]; then
        _USAGE 'Missing required configuration option(s)'
    fi
fi

## detect system command length limit

if ! CHECK_CMD_LENGTH "${_CMD_EXPORT_THREADS[*]}"; then
    _REMAINING_LEN=$((_ENV_LIMIT / 19))
    _ERR_TEXT=(
        'Command length limit would be exceeded with current configuration.'
        'This is likely due to the number of threads desired to be exported.'
        "A maximum of $_THREADS_AVAIL thread IDs are supported on this system."
    )
    _LOG error "${_ERR_TEXT[@]}"
    WAIT_UNTIL_LOOP 78
fi
