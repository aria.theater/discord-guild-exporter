#!/bin/bash -ae

_TODAY=$(date '+%Y-%m-%d')
_NOW=$(date '+%s')

if [[ ! "$CONTAINERIZED" || "$CONTAINERIZED" = false ]]; then
    _STATE_FILE="$_SOURCE_DIR_PATH/config/last-run.state"
elif [[ "$CONTAINERIZED" = true ]]; then
    _STATE_FILE="$CONFIG_PATH/last-run.state"
fi

if [ ! "$DISCORD_EXPORT_ARGS" ]; then
  DISCORD_EXPORT_ARGS=(
    '-f' 'PlainText'
    '--media'
  )
else
  DISCORD_EXPORT_ARGS=("$DISCORD_EXPORT_ARGS")
fi

if [ "$DEBUG" ]; then
  DISCORD_CHANNEL_IDS=("$DISCORD_CHANNEL_IDS")
  _EXPORT_GUILD_CMD=(
    'discord-chat-exporter-cli'
    'export' '-c' ${DISCORD_CHANNEL_IDS[@]}
    ${DISCORD_EXPORT_ARGS[@]}
    '-o' "$LOG_PATH/work/channels/"
  )
else
  _EXPORT_GUILD_CMD=(
    'discord-chat-exporter-cli' 'exportguild'
    '-g' "$DISCORD_GUILD_ID"
    ${DISCORD_EXPORT_ARGS[@]}
    '-o' "$LOG_PATH/work/channels/"
  )
fi

if [ "$DISCORD_THREAD_IDS" ]; then
  DISCORD_THREAD_IDS=("$DISCORD_THREAD_IDS")
fi

if [ ${#DISCORD_THREAD_IDS[@]} -gt 0 ]; then
  _EXPORT_THREADS_CMD=(
    'discord-chat-exporter-cli' 'export'
    '-c' ${DISCORD_THREAD_IDS[@]}
    ${DISCORD_EXPORT_ARGS[@]}
    '-o' "$LOG_PATH/work/threads/"
  )
fi
