#!/bin/bash -ae

function _FAIL_JOB () {
  local _FAIL_TEXT
  _FAIL_TEXT=("$1")
  if [ "$3" ]; then
    _FAIL_TEXT+=("(${2}s run time)")
  fi

  _LOG error "${_FAIL_TEXT[*]}"
  # if [ "$SNS_TOPIC_ARN" ]; then
  #   SNS_DISPATCH "$1" "${_FAIL_TEXT[*]}" "$SNS_ARN"
  # fi
  DISCORD_DISPATCH "${_FAIL_TEXT[*]}"

  # we intentionally don't clean up as to
  # allow time for the operator to investigate
  WAIT_UNTIL_LOOP 1
}

SECONDS=0

_LOG info 'Exporting channels...'; echo
if ! "${_EXPORT_GUILD_CMD[@]}"; then
  _FAIL_JOB \
    'Failed to export channels!' \
    "$SECONDS"
fi

if [ "$_EXPORT_THREADS_CMD" ]; then
  _LOG info 'Exporting threads...'; echo
  if ! "${_EXPORT_THREADS_CMD[@]}"; then
    _FAIL_JOB \
      'Failed to archive threads!' \
      "$SECONDS"
  fi
fi

_JOB_RUN_TIME=$SECONDS

echo $_NOW > "$_STATE_FILE"
