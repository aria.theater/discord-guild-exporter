#!/bin/bash -ae

_LOGSET_SIZE="$(_GET_DIR_SIZE 'done')"
_LOGSET_SIZE_MiB=$((_LOGSET_SIZE / 1000 / 1000))
_SUCCESS_TEXT="Successfully exported ${_LOGSET_SIZE_MiB} MiB in ${_JOB_RUN_TIME}s."

# if [ "$SNS_TOPIC_ARN" ]; then
#     _SUCCESS_SUBJECT="Guild archive successful - $DISCORD_GUILD_ID/$_TODAY"
#     SNS_DISPATCH "$_SUCCESS_SUBJECT" "${_SUCCESS_TEXT[*]}" "$SNS_TOPIC_ARN"
# fi

DISCORD_DISPATCH "$_SUCCESS_TEXT"
