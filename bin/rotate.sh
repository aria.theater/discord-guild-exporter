#!/bin/bash -ae

if COMPARE_DIR_SIZES 'work' 'done'; then
    if [[ -d "$LOG_PATH/done" ]]; then
        rm -r "$LOG_PATH/done/"
    fi
    mv "$LOG_PATH/work" "$LOG_PATH/done"
    mkdir -p "$LOG_PATH/done"
else
    _WARN_TEXT=(
		'Latest export is _smaller_ than the last.'
        'Validate and rotate manually.'
    )
    _LOG warning "${_WARN_TEXT[*]}"
	DISCORD_DISPATCH "${_WARN_TEXT[*]}"
    WAIT_UNTIL_LOOP 0
fi
