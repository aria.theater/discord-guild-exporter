#!/bin/bash -ae

## don't run if it isn't time to

if [ -f "$_STATE_FILE" ]; then
  _LAST_RUN=$(cat "$_STATE_FILE" 2>/dev/null)
  _SINCE_LAST_RUN=$((_NOW - _LAST_RUN))
  _RUN_INTERVAL_SECS=$((60 * 60 * 24 * "$RUN_INTERVAL"))
  if [[ "$_SINCE_LAST_RUN" -le $_RUN_INTERVAL_SECS && ! "$DEBUG" ]]; then
    _LOG info 'Not scheduled to run yet.'
    WAIT_UNTIL_LOOP 0
  fi
fi

## detect past failure

if
  ! (ls -1r "$LOG_PATH/work/channels" >/dev/null 2>&1) \
  || ! (ls -1 "$LOG_PATH/work/threads" >/dev/null 2>&1)
then
  _WARN_TEXT='Last archive interrupted or failed. Cleaning up before starting.'
  _LOG warning "$_WARN_TEXT"
  DISCORD_DISPATCH "$_WARN_TEXT"
  unset _WARN_TEXT
  rm -r \
    $LOG_PATH/work/channels/* \
    $LOG_PATH/work/threads/* \
    2>/dev/null
fi
