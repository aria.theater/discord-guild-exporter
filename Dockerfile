FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

# Copy DCE source files
ARG DCE_SRC=/lib/discord-chat-exporter-cli
COPY $DCE_SRC/favicon.ico ./
COPY $DCE_SRC/NuGet.config ./
COPY $DCE_SRC/Directory.Build.props ./
COPY $DCE_SRC/DiscordChatExporter.Core ./DiscordChatExporter.Core
COPY $DCE_SRC/DiscordChatExporter.Cli ./DiscordChatExporter.Cli

# Build DCE
RUN dotnet publish DiscordChatExporter.Cli --configuration Release --output ./publish

FROM mcr.microsoft.com/dotnet/runtime:6.0-jammy AS deps

LABEL   source="https://gitlab.com/aria.theater/discord-chat-exporter" \
        maintainer="erin"

# Install DCE
COPY --from=build /publish /opt/discord-chat-exporter-cli/
ADD bin/discord-chat-exporter-cli /usr/local/bin/
RUN chmod +x /usr/local/bin/discord-chat-exporter-cli

# Install OS deps
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install -y \
        supervisor \
        curl jq \
        python3-pip \
    && apt-get autoremove \
    && apt-get autoclean \
    && rm -rf /var/lib/apt-get/lists/*

# Install credstash
ARG PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_PYTHON_VERSION_WARNING=true \
    PIP_NO_CACHE_DIR=true \
    PIP_NO_COLOR=true
RUN python3 -m pip install --upgrade pip
ARG PIP_DISABLE_PIP_VERSION_CHECK=true \
    PIP_PRE=true \
    CREDSTASH_VERSION
RUN python3 -m pip install \
        credstash==$CREDSTASH_VERSION

FROM deps AS app

WORKDIR /usr/src/discord-guild-exporter

# Install application
ADD lib ./lib
ADD bin ./bin
ADD cmd.sh ./

# Configure supervisor
ADD ./supervisor.conf /etc/supervisor/conf.d/discord-guild-exporter.conf
ADD ./supervisord-watchdog /etc/supervisor/
RUN chmod +x cmd.sh /etc/supervisor/supervisord-watchdog \
    && mkdir /var/lib/discord-guild-exporter/

# Run supervisor
CMD ["/usr/bin/supervisord"]
