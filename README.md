# discord-guild-exporter

This is a hacky set of shell scripts that wrap `DiscordChatExporter` and run containerized in a loop to archive a Discord guild on a schedule.

It is an interim step for ensuring the continuity of chats in the event of disaster while work to bake this into [cables](https://gitlab.com/aria.theater/cables) is pending.

We consider this a hack because:

1) It's written purely in `bash`
2) Thread IDs must be manually maintained

## Running

Copy `.env.example` to `.env`
- Set the `LOCAL_LOG_PATH` to where you'd like exports to be saved

Copy `config/discord.env.example` to `config/discord.env`
- Set `DISCORD_TOKEN` or `DISCORD_BOT_TOKEN` to a user or bot authentication token, respectively
- Set `DISCORD_GUILD_ID` to the ID of the target guild to be exported
- Set `DISCORD_WEBHOOK_URI` to an incoming channel webhook you'd like notifications to go to
- Optionally, set `DISCORD_THREAD_IDS` to a space-separate string list of thread IDs

Bring up the container with `docker-compose`

```sh
docker-compose up --build -d
```

Watch logs to make sure everything is working

```sh
docker logs --follow discord-guild-exporter-app-1
```

By default, the program will export your guild once every week. It runs on a loop every hour. Changes can be made to `config/discord.env` between runs, and will be picked up by the app on next loop iteration.