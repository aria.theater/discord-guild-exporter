#!/bin/bash -ae

_BOLD=$(tput bold)
_NORMAL=$(tput sgr0)
function _LOG () {
	local _NOW; _NOW="$(date --rfc-3339=seconds)"
    local _LEVEL
	test "$1" = 'info' && _LEVEL=INFO
	test "$1" = 'error' && _LEVEL=ERROR
    test "$1" = 'warn' && _LEVEL=WARNING
	test ! "$_LEVEL" && _LEVEL="$1"

	if [ "$_LEVEL" = 'ERROR' ]
		then printf '%s\t%s\t%s%s%s\n' "$_NOW" "$_LEVEL" "${_BOLD}" "$2" "${_NORMAL}" >&2
		else printf '%s\t%s\t%s%s%s\n' "$_NOW" "$_LEVEL" "${_BOLD}" "$2" "${_NORMAL}"
  fi
}

function _USAGE () {
    _LOG error "$1"
    exit 64
}