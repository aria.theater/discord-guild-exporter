#!/bin/bash -ae

function WAIT_UNTIL_LOOP () {
    if [ ! "$DEBUG" ]; then
        sleep $((60 * 60))
    fi

    if [ "$1" ]; then
        exit "$1"
    else
        exit 0
    fi
}
