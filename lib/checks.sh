#!/bin/bash -ea

_GET_DIR_SIZE () {
    local _CMD
    _CMD=(
        'du'
        '--apparent-size'
        '--bytes'
        '--summarize'
        "$LOG_PATH/$1"
    )
    _CMD_RESP="$("${_CMD[@]}")"
    if [ "$_CMD_RESP" ]; then
        echo "$_CMD_RESP" | awk '{ print $1 }'
    else
        return 70
    fi
}

COMPARE_DIR_SIZES () {
    if [ $# -ne 2 ]; then
        if [ "$DEBUG" ]; then
            _LOG error "$0: Takes exactly two arguments that are directory names"
        fi
        exit 70
    fi

    _LOGSET_LAST_SIZE="$(_GET_DIR_SIZE "$2")"
    _LOGSET_NEW_SIZE="$(_GET_DIR_SIZE "$1")"
    if [ "$_LOGSET_NEW_SIZE" -lt "$_LOGSET_LAST_SIZE" ]; then
        return 1
    fi
    return 0
}

# CHECK_AWS_REGION () {
#     if nc -n -z -w2 169.254.169.254 80; then
#         _IMDS_CMD=(
#             'curl'
#             '--silent'
#             '--fail'
#             '--retry' 3
#             '--retry-max-time' 10
#             'http://169.254.169.254/latest/meta-data/placement/availability-zone'
#         )
#         _IMDS_RESP=$("${_IMDS_CMD[@]}")
#         if [ "$_IMDS_RESP" ]; then
#             EC2_REGION=${_IMDS_RESP/%[a-f]/}
#         fi
#     fi

#     local _ARN; _ARN="${_ARGS[0]}"
#     SNS_REGION="$(echo $_ARN | cut -d: -f4)"
#     if [[
#         "$DEBUG" \
#         && "$EC2_REGION" \
#         && "$EC2_REGION" != "$SNS_REGION" \
#     ]]; then
#         return 70
#     fi
# }

CHECK_CMD_LENGTH () {
    if [ $# -eq 0 ]; then
        return 70
    fi

    local _ENV
    local _CMDS_STR
    local _CMDS_LEN

    _ENV="$(env)"
    _ENV_LEN=${#_ENV}
    _ARGS_MAX=$(getconf -a | grep -e '^ARG_MAX' | awk '{print $2}')
    _CMD_STR="$1"
    _CMD_LEN=$((_ENV_LEN + ${#_CMD_STR}))
    _ENV_LIMIT=$((_ARGS_MAX - _CMD_LEN))
    if [ $_CMD_LEN -gt "$_ARGS_MAX" ]; then
        return 1
    fi
}
