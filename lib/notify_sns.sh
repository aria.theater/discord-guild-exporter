#!/bin/bash -ae

function SNS_DISPATCH () {
    if ! _SNS_PUBLISH \
        "$1" \
        "$SNS_ARN"
    then
        _LOG error "Failed to publish SNS message via $SNS_ARN"
    fi
}

# inputs: SUBJECT PATH-OR-MESSAGE TOPIC-ARN
# outputs: SNS service response
function _SNS_PUBLISH () {
	local _ARGS; _ARGS=("$@")
    if [[ $# -lt 2 || $# -gt 3 ]]; then
        if [ "$DEBUG" ]; then
            _LOG error "Usage: $0 [subject] path-or-message sns-topic-arn"
        fi
        return 70
    fi

    local SUBJECT
    if [ "$#" -eq 2 ]; then
        SUBJECT="Scheduled task ($0) notification"
    else
        SUBJECT="${_ARGS[0]}"
        shift
    fi

    local BODY
    if [ -f "${_ARGS[1]}" ]; then
        BODY="file://${_ARGS[0]}"
    else
	    BODY="${_ARGS[1]}"
    fi
    shift

    declare -a _AWSCLI_ARGS
    _AWSCLI_ARGS=(
        '--region' "$_SNS_REGION"
    )

    # Publish to SNS topic to notify admin
    _ARN=${_ARGS[0]}
    aws "${_AWSCLI_ARGS[@]}" sns publish \
        --topic-arn "$_ARN" \
        --subject "$SUBJECT" \
        --message "$BODY"
}
