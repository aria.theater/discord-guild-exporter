#!/bin/bash

function DISCORD_DISPATCH () {
    if ! _DISCORD_POST \
        "$DISCORD_WEBHOOK_URI" \
        "$1"
    then
        _LOG error "Failed to send Discord message via webhook"
    fi
}

# inputs: [discord-webhook-uri] message
# outputs: Discord message
function _DISCORD_POST () {
	local _ARGS; _ARGS=("$@")
    if [[ $# -lt 1 || $# -gt 2 ]]; then
        if [ "$DEBUG" ]; then
            _LOG error "Usage: $0 discord-webhook-uri message"
        fi
        return 70
    fi

    if [ "$#" -eq 2 ]; then
        DISCORD_WEBHOOK_URI="${_ARGS[0]}"
        shift
    fi

    bash "$_SOURCE_DIR_PATH/lib/discord.sh/discord.sh" \
        --webhook-url "$DISCORD_WEBHOOK_URI" \
        --text "$1"
}
